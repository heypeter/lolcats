<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodosTable extends Migration
{
    public function up() {
        Schema::create('todos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('desc');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('todos');
    }
}